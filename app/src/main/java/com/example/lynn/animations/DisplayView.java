package com.example.lynn.animations;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import static com.example.lynn.animations.MainActivity.imageView;

/**
 * Created by lynn on 6/27/2015.
 */
public class DisplayView extends RelativeLayout {

    public DisplayView(Context context) {
        super(context);

        imageView = new ImageView(context);

        Drawable image = getResources().getDrawable(R.drawable.alf1);

        imageView.setImageDrawable(image);

        addView(imageView);


    }

}
