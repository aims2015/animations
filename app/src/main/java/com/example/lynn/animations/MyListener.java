package com.example.lynn.animations;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.Toast;

import static com.example.lynn.animations.MainActivity.*;

/**
 * Created by lynn on 6/11/2015.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        String errors = "";

        String[] parameters = MainActivity.parameters.getText().toString().split(",");

        if (MainActivity.parameters.length() == 0)
            errors += "The parameter list is empty ";

        int[] theParameters = new int[parameters.length];

        for (int counter=0;counter<theParameters.length;counter++)
            theParameters[counter] = Integer.parseInt(parameters[counter]);

        int duration = -1;

        try {
            duration = Integer.parseInt(MainActivity.duration.getText().toString());
        } catch (Exception e) {
            errors += "The duration is not an integer ";
        }

        ObjectAnimator animator = null;

        if (errors.equals("")) {
            String property = animations.getSelectedItem().toString();

            if (parameters.length == 1)
                animator = ObjectAnimator.ofFloat(imageView, property, theParameters[0]);
            else if (parameters.length == 2)
                animator = ObjectAnimator.ofFloat(imageView, property, theParameters[0], theParameters[1]);
            else if (parameters.length == 3)
                animator = ObjectAnimator.ofFloat(imageView, property, theParameters[0], theParameters[1], theParameters[2]);

            animator.setDuration(duration*1000);

            animator.start();
        } else
            Toast.makeText(v.getContext(),errors,Toast.LENGTH_LONG).show();
    }

}
