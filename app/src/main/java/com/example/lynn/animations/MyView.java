package com.example.lynn.animations;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.lynn.animations.MainActivity.*;


/**
 * Created by lynn on 6/11/2015.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {

        super(context);

        String[] list = {"translationX","translationY","scaleX","scaleY","scale","rotation",
                         "rotationX","rotationY","alpha"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,android.R.layout.simple_spinner_item,list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        animations = new Spinner(context);

        animations.setAdapter(adapter);

        animations.setId(R.id.animations);

        addView(animations);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.animations);

        animate = new Button(context);

        animate.setText("Animate");

        animate.setId(R.id.animate);

        animate.setLayoutParams(layoutParams);

        animate.setOnClickListener(listener);

        addView(animate);

        TextView textView = new TextView(context);

        textView.setText("Specify up to 3 parameters seperated by commas");

        textView.setId(R.id.parameterslabel);

        layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.animate);

        textView.setLayoutParams(layoutParams);

        addView(textView);

        parameters = new EditText(context);

        layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.parameterslabel);

        parameters.setId(R.id.parameters);

        parameters.setLayoutParams(layoutParams);

        addView(parameters);

        TextView textView1 = new TextView(context);

        textView1.setText("Duration in seconds");

        textView1.setId(R.id.durationlabel);

        layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.parameters);

        textView1.setLayoutParams(layoutParams);

        addView(textView1);

        duration = new EditText(context);

        layoutParams = new RelativeLayout.LayoutParams(100,LayoutParams.WRAP_CONTENT);

        layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.durationlabel);

        duration.setLayoutParams(layoutParams);

        duration.setId(R.id.duration);

        addView(duration);

        displayView = new DisplayView(context);

        layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,600);

        layoutParams.addRule(RelativeLayout.BELOW,R.id.animations);
        layoutParams.addRule(RelativeLayout.BELOW,R.id.animate);
        layoutParams.addRule(RelativeLayout.BELOW,R.id.parameterslabel);
        layoutParams.addRule(RelativeLayout.BELOW,R.id.parameters);
        layoutParams.addRule(RelativeLayout.BELOW,R.id.durationlabel);
        layoutParams.addRule(RelativeLayout.BELOW,R.id.duration);

        displayView.setLayoutParams(layoutParams);

        addView(displayView);
    }

}
